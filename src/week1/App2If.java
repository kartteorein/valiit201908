package week1;

public class App2If {
    public static void main(String[] args) {
        int temperature = Integer.parseInt(args[0]);

        if (temperature >= 30) {
            System.out.println("Ilm on ilus!");
        } else if (temperature >= 20) {
            System.out.println("Ilm on normaalne.");
        } else if (temperature >= 15) {
            System.out.println("Väljas on jaani-ilm.");
        } else {
            System.out.println("Ilm ei ole ilus!");
        }


// Variant 2
// 1 - roheline tuli
// 2 - kollane tuli
// 3 - punane tuli
        int inputValue = 0;

        switch (inputValue) {
            case 1:
                // Tee midagi
                System.out.println("Fooris põleb roheline tuli!");
                break;
            case 2:
                // Tee midagi muud
                System.out.println("Fooris oõleb kollane tuli!");
                break;
            case 3:
                // Tee midagi hoopis muud
                System.out.println("Fooris põleb punane tuli!");
                break;
            default:
                // Siia satud sa siis, kui eelnevad cased ei olnud tõesed
                System.out.println("Valgusfoor on rikkis!");
        }


        // Variant 3 (inline-if)
        // String muutuja = Kas tingimus on tõene? "jah" : "ei";
        String weatherText = temperature >= 30 ? "ilm on ilus" : "Ilm ei ole ilus,";
    }

}
