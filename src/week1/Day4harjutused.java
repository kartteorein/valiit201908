package week1;

import java.util.LinkedList;
import java.util.*;

public class Day4harjutused {
    public static void main(String[] args) {

////        List
//        List<String> names = new ArrayList<>();
//        names.add("Thomas");
//        names.add("Mary");
//        names.add("Linda");
//        System.out.println(names);
//
////       Mis kohal asub Linda?
//        int insertPosition = (names.indexOf("Linda"));
//        System.out.println(insertPosition);
//
////        Lisa etteantud indeksi kohale uus väärtus, järgmised väärtused nihkuvad edasi.
//        names.add(2, "Ruth");
//        names.add(insertPosition, "Ruth");
//        System.out.println(names);
//
////        Kas antud väärtus on juba listis olemas?
//        System.out.println(names.contains("Linda"));
//        System.out.println("Joe");
//
////        Asenda element indeksi kohal X...
//        int lindaPosition = names.indexOf("Linda");
//        if (lindaPosition >= 0) {
//            names.set(lindaPosition, "Laura");
//        }
//        System.out.println(names);
//
//        for (String name : names) {
//            System.out.println("Nimi: " + name);
//        }
//        System.out.println(names.get(3)); // Kindla indeksiga elemendi pärimine.
//
////        Mitmetasandiline list
//        List<List<String>> complexList = new ArrayList<>();
//
//        List<String> humans = Arrays.asList("Mark", "Mary", "Thomas", "Linda");
//        complexList.add(humans);
//
//        List<String> cars = Arrays.asList("BMW", "Renault", "Open", "Ford");
//        complexList.add(cars);
//
//        complexList.add(Arrays.asList("Tupolev", "Jakovlev", "Boeing", "Airbus"));
//
//        System.out.println(complexList);
//        System.out.println(complexList.get(2).get(1));
//
////        Set
//
//        Set<String> uniqueNames = new HashSet<>();
//        uniqueNames.add("Mary");
//        uniqueNames.add("Thomas");
//        uniqueNames.add("Mark");
//        uniqueNames.add("Mary");
//
//        System.out.println(uniqueNames);
//        Object[] uniqueNamesArr = uniqueNames.toArray();
//
//        for (String uniqueName : uniqueNames) {
//            System.out.println("Unikaalne nimi: " + uniqueNames);
//        }
//
//        uniqueNames.remove("Mark");
//        System.out.println(uniqueNames);
//
////        uniqueNames.size()
//
//        Comparator<String> nameComparator = new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return o2.compareTo(o1);
//            }
//        };
//
//        Set<String> sortedNames = new TreeSet<>(nameComparator);
//        sortedNames.add("Mary");
//        sortedNames.add("Thomas");
//        sortedNames.add("Mark");
//        System.out.println(sortedNames);
//
////        Map - võtme ja väärtuse paaride kogum.
//
        Map<String, String> phoneNumbers = new HashMap<>();
        phoneNumbers.put("Mati", "+37255 786 234");
        phoneNumbers.put("Kati", "+37250 166 335");
        phoneNumbers.put("Mari", "+37250 166 335");

        for (String key : phoneNumbers.keySet()) {
            System.out.println("Sõber: " + key + ", telefon: " + phoneNumbers.get(key));

//        }
//        phoneNumbers.put("Mari", "004867890965644");
//        phoneNumbers.put("Leonardo", "003858734565644");
//        System.out.println(phoneNumbers);
//
//        Map<String, Map<String, String>> contacts = new TreeMap<>();
//
//        Map<String, String> mariContactDetails = new TreeMap<>();
//        mariContactDetails.put("Phone", "34655546654654");
//        mariContactDetails.put("Email", ",mari@kari.ee");
//        mariContactDetails.put("Address", "Laeva 9-15, Elva");
//        contacts.put("Mari", mariContactDetails);
//
//        Map<String, String> martContactDetails = new TreeMap<>();
//        martContactDetails.put("Phone", "34655546654654");
//        martContactDetails.put("Email", ",mari@kari.ee");
//        martContactDetails.put("Address", "Laeva 9-15, Elva");
//        contacts.put("Mart", mariContactDetails);
//
//        System.out.println(contacts);
//        System.out.println("Mardi telefoninumber : "+ contacts.get("Mart").get("Phone"));

// Ülesanne 19: ArrayList

//        List<String> cities = new ArrayList<>();
//        cities.add("Tartu");
//        cities.add("Tallinn");
//        cities.add("Pärnu");
//        cities.add("Narva");
//        cities.add("Tapa");
//        System.out.println(cities.get(1));
//        System.out.println(cities.get(3));
//        System.out.println(cities.get(5));

//        Ülesanne 20

        Queue<String> names = new LinkedList<>();
      names.add("Mary");
       names.add("Mari");
        names.add("Mart");
        names.add("Mark");
        names.add("Marin");
        names.add("Marko");
        while(names.isEmpty()) {
            System.out.println("Tõmbasin queuest sellise nime:" + names.remove());
        }

//        Ülesanne 21

        Set<String> nameSet = new TreeSet<>();

        names.add("Mary");
        names.add("Mari");
        names.add("Mart");
        names.add("Mark");
        names.add("Marin");
        names.add("Marko");
        names.forEach(myNameToPrint -> System.out.println("Lambda-avaldis prindib: " + myNameToPrint));

    }

//    Ülesanne 22



    Map<String, String[]> countryCities = new HashMap<>();
    countryCities.put("Estonia", new String[]{"Tallinn", "Tartu", "Valga", "Võru"});
    countryCities.put("Sweden",  new String[]{"Stockholm", "Uppsala", "Lund", "Köping"});
    countryCities.put("Finland", new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"});

    for (String country : countryCities.keySet()) {
        System.out.println("Country: " + country);
        String[] currentCountryCities = countryCities.get(country);
        System.out.println("Cities: ");
        for (String city : currentCountryCities) {
            System.out.println("\t" + city);
        }

    }
}
    }
