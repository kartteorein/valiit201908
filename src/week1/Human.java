package week1;

public class Human {

    public String name;
    public int age;
    public double weight;

    public Human(String name, int age, double weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public Human(String name) {
        this.name = name;

    }
    public Human() {

    }
    public boolean isYoung() {
        return age < 100;
    }

    public static boolean isThisHumanYoung(Human human){
        return human.age < 100;
    }
}
