package week1;

public class MonthPrint {
    public static void main(String[] args) {
        // Lugege käsurea parameetrina sisse number vahemikus 1 .. 12
        // ja printige ekraanile sellele numbrile vastav kuu.
        // näiteks "2" --> veebruar, "7" --> juuli

        int monthNumber = Integer.parseInt(args[0]);
        switch (monthNumber) {
            case 1:
                System.out.println("jaanuar");
                break;
            case 2:
                System.out.println("veebruar");
                break;
            case 3:
                System.out.println("märts");
                break;
            case 4:
                System.out.println("aprill");
                break;
            case 5:
                System.out.println("mai");
                break;
            case 6:
                System.out.println("juuni");
                break;
            case 7:
                System.out.println("juuli");
                break;
            case 8:
                System.out.println("august");
                break;
            case 9:
                System.out.println("september");
                break;
            case 10:
                System.out.println("oktoober");
                break;
            case 11:
                System.out.println("november");
                break;
            case 12:
                System.out.println("detsember");
                break;
            default:
                System.out.println("VIGA: Sellist kuud pole olemas");
        }
    }
}
