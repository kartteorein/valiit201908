package week1;

public class Massiivid {

    public static void main(String[] args) {

        // Ülesanne 7 - defineeri muutuja, mille tüübiks täisarvude massiiv

        int [] myIntArr;
        myIntArr = new int [5];
        myIntArr[0] = 1;
        myIntArr[1] = 2;
        myIntArr[2] = 3;
        myIntArr [3] = 4;
        myIntArr [4] = 5;

        System.out.println(String.format("Massiivi esimese elemendi väärtus: %s", myIntArr[0]));
        System.out.println("Massiivi kolmanda elemendi väärtus: " + myIntArr[2]);
        System.out.println("Massiivi viimase elemendi väärtus " + myIntArr[myIntArr.length - 1]);

        // Ülesanne 8 - defineeri muutuja, mille tüübiks tekstide massiiv

        String[] cities = {"Tallinn", "Helsinki", "Madrid", "Paris"};

        // Ülesanne 9 - defineeri kahetasandiline massiiv

        int [][] myTwoDimensionArray = {{1,2,3}, {4,5,6}, {7,8,9,0}};

        // Ülesanne 10 - defineeri kahetasandiline massiiv

            String[][] countryCities = new String[3][];
            countryCities[0] = new String[] {"Tallinn", "Tartu", "Valga", "Võru"};
            countryCities[1] = new String[] {"Stockholm", "Uppsala", "Lund", "Köping"};
            countryCities[2] = new String[] {"Helsinki", "Espoo", "Hanko", "Jämsä"};

            // Väärtusta massiv element-haaval, inline-põhimõttel

           String[][] countryCities2 = {{"Tallinn", "Tartu", "Valga", "Võru"}, {"Stockholm", "Uppsala", "Lund", "Köping"}, {"Helsinki", "Espoo", "Hanko", "Jämsä"}};

    }

}
