package week1;

public class App2 {
    private static Object sout;

    public static void main(String[] args) {
        byte b1 = 1;  // Suurus: 1 bait. Väike number.
        char c1 = 'B';
        short s1 = 4578;
//        int i1 =

        // Operaatorid

        // = =

        int i4 = 5;
        int i5 = 2;
        int i6 = 5;
        boolean test1 = (i4 == i5);
        System.out.println(i4 == i5);
        System.out.println(i4 = i6);


        // ++

        int i7= 50;
        System.out.println(i7++);
        System.out.println(i7);

        int i8 = 60;
        System.out.println(++i8);

        // -=

        int i9 = 11;
        i9 = i9 + 5; // Variant 1
        i9 += 5; // Variant 2

        // Loogiline või (//)
        // Üls või teine või mõlemad;

        boolean b10 = false;
        boolean b11 = true;
        System.out.println(b10); // b11);

        // Loogiline and &&

        boolean b12 = true;
        boolean b13 = true;
        System.out.println(b12 && b13);

        // Mooduliga jagamine

        int i14 = 11;
        int i15 = 10;
        System.out.println(i14 % 10);
        System.out.println(i15 % 10);

        // Wrapper klassid

        Integer i20 = 20;
        Integer i21 = new Integer(20);

        // Harjutus 1

        int a = 1;
        int b = 1;
        int c = 3;

       // Prindi välja a == b ja a == c

        System.out.println(a == b);
        System.out.println(a == c);
        // Lisa koodi rida a = c
        a = c;

        // Harjutus 2

        int x1 = 10;
        int x2 = 20;

        int y1 = ++x1;
        System.out.println("x1: " + x1);
        System.out.println("y1: " + y1);

        int y2 = x2++;

        System.out.println(x2);
        System.out.println(y2);

        // Stringitöötlus

        System.out.println("Isa ütles: \"Tule siia\"");
        System.out.println("Minu faili asukoht: C:\\test\\test.txt");
        System.out.println("See on esimene rida. \nSee on teine rida.");

        String myText1 = "This is a text.";
        String myText2 = "This is another text.";
        myText1 = myText1 + myText2;

        String myText3 = "T3";
        String myText4 = "T3";
        System.out.println(myText3 == myText4);  // Ebakorrektne viis stringide võrdlemiseks.
        System.out.println(myText3.equals(myText4)); // Korrektne viis stringide võrdlemiseks.
        System.out.println(myText3.equalsIgnoreCase("t3"));
        String myText5 = new String("Some text...");

        System.out.println("Hello, World!");
        System.out.println("Hello, \"World!\"");
        System.out.println("Steven Hawking once said: \"Life would be tragic if it weren't funny\"" );
        System.out.println("Kui liita kokku sõned \"See on teksti esimene pool\" ning \"See on" +
                "on teksti teine pool\", siis tulemuseks saame \"See on teksti esimene pool See on teksti teine pool\".");
        System.out.println("Elu on ilus.");
        System.out.println("Elu on 'ilus'");
        System.out.println("Elu on \"ilus\".");
        System.out.println("Kõige rohkem segadust tekitab \"-märgi kasutamine sõne sees.");
        System.out.println("Eesti keele kõige ilusam lause on: \"Sõida tasa üle silla!\"");
        System.out.println("'Kolm' - kolm, 'neli' - neli, \"viis\" - viis.");

//       String tallinnPopulation = ("450 000" + tallinnPopulation + " inimest.");
        int populationOfTallinn = 450_000;
        System.out.println("Tallinns elab " + populationOfTallinn + " inimest.");

        String planet1 = "Merkuur";
        String planet2 = "Veenus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uraan";
        String planet8 = "Neptuun";
        int planetCount = 8;
        System.out.println(String.format("%x, %s, %s, %s, %s, %s, %s, %s on Päikesesüsteemi %d planeeti.", planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount));

        // Ülesanne 2

        String bookTitle = "Rehepapp";
        System.out.println(String.format("Raamatu \"%s\" autor on Andrus Kivirähk.", bookTitle));

    }
}
