package week1;

import java.util.Scanner;

public class App3 {

    public static final double VAT_RATE = 0.3;

    public static void main(String[] args) {

        Scanner poemInput = new Scanner(System.in);
        System.out.println("Kirjuta üks ilus luuletus:\n");
        String poem = poemInput.nextLine();
        System.out.println("Luuletus oli selline:n");
        System.out.println(poem);

        // Vajaliku mälu efektiivse kasutamise jaoks. Mõistlik suurte stringide kokkupanemisel.

//        StringBuilder myStringBuilder = new StringBuilder(); // append meetod - lisab uue teksti loodava teksti lõppu
//        myStringBuilder.append("Marek, ");
//        myStringBuilder.append("Rein, ");
//        myStringBuilder.append("Liina, ");
//        String allTheNames = myStringBuilder.toString();
//        System.out.println(allTheNames);


       // String myText = "Tallinn, Tartu, Valga, Rapla";
        //Scanner scanner = new Scanner(myText);
//        scanner.useDelimiter(", ");
//        while(scanner.hasNext()) {
//            System.out.println(scanner.next());
//        }
//
//    }
//
//
//    public static void main(String[] args) {
//        String text1 = "Tere";
//        String text2 = "3";
//
//       Integer num1 = Integer.parseInt(text2);
//       // String text3 = num1.toString();
//        String text3 = String.valueOf(num1);
//
//        int myBigNumber = 777;
//        byte mySmallNumber = (byte) myBigNumber;
//        System.out.println(mySmallNumber);



    }
}
