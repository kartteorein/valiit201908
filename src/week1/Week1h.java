package week1;

import java.util.*;

public class Week1h {

    // Ülesanne 1

    public static void main(String[] args) {
        //      for (int row = 6; row >= 1; row--) {  // sideLength = 6
        //           for (int col = 1; col <= row; col++) {
//                System.out.print("#");
//            }
//            System.out.println();
//        }
//
//        // Ülesanne 2
//
//        for (int row = 1; row <= 6; row++) {
//            for (int col = 1; col <= row; col++) {
//                System.out.print("#");
//            }
//            System.out.println();
//        }
//
        // Ülesanne 3


//        for (int row = 1; row <= 5; row++) {
//            for (int col = 1; col <= 5 - row; col++) {
//               System.out.print(" ");
//            }
//            for (int k = 1; k <= row; k++) {
//                System.out.print("@");
//            }
//            System.out.println("");
//        }

        // Ülesanne 4

//                int number = 123567;
//                String numberText = String.valueOf(number); //"1234567"
//                char[] numberChars = numberText.toCharArray(); // "1324567" -> {'1', '2', '3', '4'. '5'. '6'. '7'}
//                String reversedNumberText = "";
//                for (int i = 0; i < numberChars.length; i++) {
//                    reversedNumberText = numberChars[i] + reversedNumberText;
//                }
//                int reversedNumber = Integer.parseInt(reversedNumberText);
//                System.out.println(reversedNumber);
//
//                System.out.println();
//                int number2 = 1234567;
//                String numberText2 = String.valueOf(number2);
//                StringBuilder numberReverseBuilder = new StringBuilder(numberText2);
//                System.out.println(reversedNumber);

//            }

// Ülesanne 5

//    String studentInfo = "Marek 99";
//    String[] studentInfoArray = studentInfo.split(" "); // {"Marek", "99"}
//                if (args.length >= 2) { // Kas on üldse parameetreid, mida analüüsida?
//                    String studentName = args[0];
//                    int studentScore = Integer.parseInt(args[1]);
//                    String resultingText = studentName + ": ";
//
//                    if (studentScore > 90) {
//                        resultingText = resultingText + "PASS - 5, " + studentScore;
//                    } else if (studentScore > 80) {
//                        resultingText = resultingText + "PASS - 4, " + studentScore;
//                    } else if (studentScore > 70) {
//                        resultingText = resultingText + "PASS - 3, " + studentScore;
//                    } else if (studentScore > 60) {
//                        resultingText = resultingText + "PASS - 2, " + studentScore;
//                    } else if (studentScore > 50) {
//                        resultingText = resultingText + "PASS - 1, " + studentScore;
//                    } else {
//                        resultingText = "FAIL";
//                    }
//                    System.out.println(resultingText);
//                }

        // Ülesanne 6

//        double[][] triangleSideList = {
//                {1.5, 7.7},
//                {14.31, 56.72},
//                {18.51, 21.67},
//                {798.45, 983.5},
//                {61.92, 7.21}
//        };
//
//        for (double[] triangleSides : triangleSideList) {
//            double c = Math.sqrt(Math.pow(triangleSides[0], 2) + Math.pow(triangleSides[1], 2)); // Math.pow(), Math.sqrt()
//            System.out.println("Kolmnurga hüpotenuusi pikkus: " + c);
//        }
//
//        // Ülesanne 7
//
        String[][] countries = {
//                {"Estonia", "Tallinn", "Jüri Ratas"},
//                {"Latvia", "Riga", "Arturs Krišjānis Kariņš"},
//                {"Lithuania", "Vilnius", "Saulius Skvernelis"},
//                {"Germany", "Berlin", "Angela Merkel"},
//                {"Sweden", "Stockholm", "Stefan Löfven"},
//                {"Finland", "Helsinki", "Antti Rinne"},
//                {"Russia", "Moscow", "Dmitri Medvedjev"},
//                {"Norway", "Oslo", "Erna Solberg"},
//                {"Denmark", "Copenhagen", "Mette Frederiksen"},
//                {"Iceland", "Reykjavik", "Katrin Jakobsdottir"},
        };
        for (String[] country : countries) {
            System.out.println(country[2]);
        }
        for (int i = 0; i < countries.length; i++) {
            System.out.println(String.format("Country: %s, Capital: %s, Prime minister: %s", countries[i][0], countries[i][1], countries[i][2]));
        }

        // Ülesanne 8

        // String on objekt.
        // Stringide massiiv on Objekt.
        // String[] arr1 = {"aa", "bb"};
        //

        Object[][] countries2 = {
                {"Estonia", "Tallinn", "Jüri Ratas", new String[]{"Estonian", "Russian"}},
                {"Latvia", "Riga", "Arturs Krišjānis Kariņš", new String[]{"Latvian", "Russian"}},
                {"Lithuania", "Vilnius", "Saulius Skvernelis", new String[]{"Lithuanian", "Polish"}},
                {"Germany", "Berlin", "Angela Merkel", new String[]{"German", "French"}},
                {"Sweden", "Stockholm", "Stefan Löfven", new String[]{"Swedish", "Sami"}},
                {"Finland", "Helsinki", "Antti Rinne", new String[]{"Finnish", "Swedish"}},
                {"Russia", "Moscow", "Dmitri Medvedjev", new String[]{"Russian", "English"}},
                {"Norway", "Oslo", "Erna Solberg", new String[]{"Norwegian", "English"}},
                {"Denmark", "Copenhagen", "Mette Frederiksen", new String[]{"Danish", "English"}},
                {"Iceland", "Reykjavik", "Katrin Jakobsdottir", new String[]{"Icelandic", "English"}},
        };

        for (Object[] country : countries2) {
            System.out.println(country[0] + ":");
            for (String language : (String[]) country[3]) {
                System.out.println(language + " ");
            }
            System.out.println();
            System.out.println();
        }

        // Ülesanne 9

//       List<List<Object>> countries3 = Arrays.asList(
//                Arrays.asList("Estonia", "Tallinn", "Jüri Ratas", Arrays.asList("Estonian", "Russian")),
//                Arrays.asList("Latvia", "Riga", "Arturs Krišjānis Kariņš", Arrays.asList("Latvian", "Russian")),
//                Arrays.asList("Lithuania", "Vilnius", "Saulius Skvernelis", Arrays.asList("Lithuanian", "Polish")),
//                Arrays.asList("Germany", "Berlin", "Angela Merkel", Arrays.asList("German", "French")),
//                Arrays.asList("Sweden", "Stockholm", "Stefan Löfven", Arrays.asList("Swedish", "Sami")),
//                Arrays.asList("Finland", "Helsinki", "Antti Rinne", Arrays.asList("Finnish", "Swedish")),
//                Arrays.asList("Russia", "Moscow", "Dmitri Medvedjev", Arrays.asList("Russian", "English")),
//                Arrays.asList("Norway", "Oslo", "Erna Solberg", Arrays.asList("Norwegian", "English")),
//                Arrays.asList("Denmark", "Copenhagen", "Mette Frederiksen", Arrays.asList("Danish", "English")),
//                Arrays.asList("Iceland", "Reykjavik", "Katrin Jakobsdottir", Arrays.asList("Icelandic", "English"))
//        );
//
//        for (List<Object> country : countries3) {
//            System.out.println(country.get(0) + ":");
//            for (String language : (List<String>) country.get(3)) {
//                System.out.print(language + " ");
//            }
//            System.out.println();
//            System.out.println();
//        }
//        System.out.println();
//    }
//
//// Harjutus 10
//
//    Comparator<String> countryNameLengthComparator = (countryName1, countryName2) -> {
//        if (countryName1.length()) == countryName2.length()) {
//            return countryName1.length() - countryName2.length();
//        } else {
//    return countryName1.length() - countryName2.length();
//        }
//    };
//
//    Map<String, List<Object>> countryMap = new HashMap<>();
//            countryMap.put ("Estonia", Arrays.asList("Tallinn", "Jüri Ratas", Arrays.asList("Estonian", "Russian")));
//            countryMap.put("Latvia", Arrays.asList("Riga", "Arturs Krišjānis Kariņš", Arrays.asList("Latvian", "Russian")));
//            countryMap.put("Lithuania", Arrays.asList("Vilnius", "Saulius Skvernelis", Arrays.asList("Lithuanian", "Polish")));
//           countryMap.put("Germany", Arrays.asList("Berlin", "Angela Merkel", Arrays.asList("German", "French"));
//            countryMap.put("Sweden", Arrays.asList("Stockholm", "Stefan Löfven", Arrays.asList("Swedish", "Sami")));
//           countryMap.put("Finland", Arrays.asList("Helsinki", "Antti Rinne", Arrays.asList("Finnish", "Swedish")));
//            countryMap.put("Russia", Arrays.asList("Moscow", "Dmitri Medvedjev", Arrays.asList("Russian", "English")));
//            countryMap.put("Norway", Arrays.asList("Oslo", "Erna Solberg", Arrays.asList("Norwegian", "English")));
//            countryMap.put("Denmark", Arrays.asList("Copenhagen", "Mette Frederiksen", Arrays.asList("Danish", "English")));
//            countryMap.put("Iceland", Arrays.asList("Reykjavik", "Katrin Jakobsdottir", Arrays.asList("Icelandic", "English")));
//
//
//            for (String countryName : countryMap.keySet()){
//        System.out.println(countryName + ":");
//        (List<String>) languages = (List<String>) countryMap.get(countryName). get(2);
//
//        for (String languages : languages) {
//            System.out.println(language + "");
//      /

    // Ülesanne 11

    Queue<List<Object>> countryQueque = new LinkedList<>();
countryQueque.add(Arrays.asList("Estonia", "Tallinn", "Jüri Ratas", Arrays.asList("Estonian", "Russian")));
countryQueque.add(Arrays.asList("Latvia", "Riga", "Arturs Krišjānis Kariņš", Arrays.asList("Latvian", "Russian")));
countryQueque.add(Arrays.asList("Lithuania", "Vilnius", "Saulius Skvernelis", Arrays.asList("Lithuanian", "Polish")));
countryQueque.add(Arrays.asList("Germany", "Berlin", "Angela Merkel", Arrays.asList("German", "French")));
countryQueque.add(Arrays.asList("Sweden", "Stockholm", "Stefan Löfven", Arrays.asList("Swedish", "Sami")));
countryQueque.add(Arrays.asList("Finland", "Helsinki", "Antti Rinne", Arrays.asList("Finnish", "Swedish")));
countryQueque.add(Arrays.asList("Russia", "Moscow", "Dmitri Medvedjev", Arrays.asList("Russian", "English")));
countryQueque.add(Arrays.asList("Norway", "Oslo", "Erna Solberg", Arrays.asList("Norwegian", "English")));
countryQueque.add(Arrays.asList("Denmark", "Copenhagen", "Mette Frederiksen", Arrays.asList("Danish", "English")));
countryQueque.add(Arrays.asList("Iceland", "Reykjavik", "Katrin Jakobsdottir", Arrays.asList("Icelandic", "English")));

//while(countryQueque.isEmpty()){
//    List<Object> country = countryQueque.remove();
//    System.out.println(country.get(0) + ": ");
//    for (String language : (List<String>country.get(3)){
//        System.out.print(language + " ");
//    }
//    System.out.println();
//    System.out.println();
}
        }





























































