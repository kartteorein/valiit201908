package week1;

import java.sql.SQLOutput;

public class ProgrammingBasics {
    public static void main(String[] args) {

        // Koodikonstruktsioonid

        // Ülesanne 2

        String city = "Berlin";
        if (city.equals("Milano")) {
            System.out.println("Ilm on soe");
        } else {
            System.out.println("Ilm polegi kõige tähtsam.");
        }

        // Ülesanne 3

        int grade = 5;
        if (grade == 1) {
            System.out.println("Nõrk");
        } else if (grade == 2) {
            System.out.println("Miiterahuldav");
        } else if (grade == 3) {
            System.out.println("Rahuldav");
        } else if (grade == 4) {
            System.out.println("Hea");
        } else if (grade == 5) {
            System.out.println("Väga hea");
        } else {
            System.out.println("VIGA: Ebakorrektne hinne!");

            // Ülesanne 4

            switch (grade) {
                case 1:
                    System.out.println("Nõrk");
                    break;
                case 2:
                    System.out.println("Mitterahuldav");
                    break;
                case 3:
                    System.out.println("Rahuldav");
                    break;
                case 4:
                    System.out.println("Hea");
                    break;
                case 5:
                    System.out.println("Väga hea");
                    break;
                default:
                    System.out.println("VIGA: Ebakorrektrne hinne!");

            }

            // Ülesanne 5

            int age = 101;
            String textToPrint = age <= 100 ? "Noor" : "Vana";
            System.out.println(age);


            // Ülesanne 6

            textToPrint = (age < 100) ? ("Noor") : ((age == 100) ? ("Peaaegu vana") : ("Vana"));
            System.out.println(textToPrint);


        }

    }
}

