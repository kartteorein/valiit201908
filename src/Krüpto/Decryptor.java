package Krüpto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Decryptor extends Cryptor {

    private Map<String, String> cryptoMap = new HashMap<>();

    @Override
    public void initAlphabet(List<String> alphabet) {
        for (String line : alphabet) {
            String[] lineParts = line.split(", ");
            this.cryptoMap.put(lineParts[1], lineParts[0]);

        }

    }

    public String convertText (String inputText) {
        String convertedText = "";

        char[] textChars = inputText.toCharArray();

        for (int i = 0; i < textChars.length; i++) {
            String charString = String.valueOf(textChars[i]);
            // cryptoMap.get()
            convertedText =  convertedText + this.cryptoMap.get(charString);
        }
        return inputText;
    }
}
