package Krüpto;

import Krüpto.Cryptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Encryptor extends Cryptor {

    private Map<String, String> cryptoMap = new HashMap<>();
    // A --> Ü
    // B --> Ö

    public void initAlphabet(List<String> alphabet) {
        // cryptoMap.keySet()
        // cryptoMap.put()
        // string.split()

        for (String line : alphabet) {
            // "A, Ü" --> "A", "Ü"
            String[] lineParts = line.split(", ");
            this.cryptoMap.put(lineParts[0], lineParts[1]);
        }
    }

    @Override
    public String convertText(String textToConvert) {
        // sisendtekst tuleb krüpteerida
        // .toCharArray()
        // String.valueOf()
        // cryptoMap.get()
        String convertedText = "";

        char[] textChars = textToConvert.toCharArray();

        for (int i = 0; i < textChars.length; i++) {
            String charString = String.valueOf(textChars[i]);
            // cryptoMap.get()
            convertedText =  convertedText + this.cryptoMap.get(charString);
        }
        return convertedText;
    }
}