package Krüpto;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Crypto {



    public static void main(String[] args) throws IOException {
        List<String> alphabet = Files.readAllLines(Paths.get(args[0]));

        Cryptor encryptor = new Encryptor();
        encryptor.initAlphabet(alphabet);
        String encryptedMessage = encryptor.convertText(args[1]);
        System.out.println("Krüpteeritud sõnum: " + encryptedMessage);

        Cryptor decryptor = new Decryptor();
        decryptor.initAlphabet(alphabet);
        String decryptedMessage = decryptor.convertText(args[1]);
        System.out.println("Dekrüpteeritud sõnum: " + decryptedMessage);




    }
}
