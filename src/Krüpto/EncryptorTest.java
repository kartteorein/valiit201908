package Krüpto;

import java.util.List;

public class Encryptor extends Cryptor {

    @Override
    public void initAlphabet(List<String> alphabet) {
        for (String line : alphabet) {
            String[] lineParts = line.split(", ");
            this.cryptoMap.put(lineParts[0], lineParts[1]);
        }
    }
}
