package Krüpto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Cryptor {



    protected Map<String, String> cryptoMap = new HashMap<>();

    public abstract void initAlphabet(List<String> alphabet);
    public abstract String convertText(String inputText);


    }


