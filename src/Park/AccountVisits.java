package Park;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Day10 {

    public static List<Visit> visitsList = new ArrayList<>();

    public static void main(String[] args) throws IOException {

        List<String> visitLines = Files.readAllLines(Path.of(args[0]));

        for (String line : visitLines) {
            String[] lineParts = line.split(", ");
            Visit v = new Visit(lineParts[0], Integer.parseInt(lineParts[1]));
            visitsList.add(v);
        }

        Comparator<Visit> instructionToSort = new Comparator<Visit>() {
            @Override
            public int compare(Visit o1, Visit o2) {
//                // Kui o1 on suurem kui o2, siis negatiivne
//                // Kui o1 on võrdne o1, siis 0
//                // Kui o1 on väiksem kui o2, siis positiivne
//                if (o2.getCount() > o1.getCount()) {
//                    return -1;
//                } else if (o2.getCount() == o1.getCount()) {
//                    return 0;
//                } else {
//                    return 1;
//                }
                return o1.getCount() - o2.getCount();
            }
        };

        Collections.sort(visitsList, instructionToSort);

        System.out.println(visitsList.get(visitsList.size() - 1).getCount() + " " + visitsList.get(visitsList.size() - 1).getDate());
    }
}

