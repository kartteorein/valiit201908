package week2;

public class CountryInfo {

    public String name;
    public String capital;
    public String primeMinister;
    public String[] languages;

    public CountryInfo(String name, String capital, String primeMinister, String[] languages) {
        this.name = name;
        this.capital = capital;
        this.primeMinister = primeMinister;
        this.languages = languages;
    }


    public String toString() {
        String info = this.name + ":\n";
        for (String language : this.languages) {
            info = info + "\t" + languages + "\n";
        }
        return info;
    }
}