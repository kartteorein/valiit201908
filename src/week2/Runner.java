package week2;

import week2.Athlete;

public class Runner extends Athlete {

    @Override
    public void perform() {
        System.out.println("Rushing like wind...");
    }
}
