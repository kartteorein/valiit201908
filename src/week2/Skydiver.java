package week2;

import week2.Athlete;

public class Skydiver extends Athlete {

    @Override
    public void perform() {
        System.out.println("Falling from the sky...");
    }
}
