//package week1;
//
//import java.math.BigInteger;
//
//public class w2d6 {
//
//    public static void main(String[] args) {
//        // Kordamisharjutused
//        double di = 456.78;
//        float f1 = 456.78F;
//
//        String t1 = "test";
//        char[] ca1 = {'t', 'e', 's', 't'};
//
//        boolean b1 = true;
//        boolean b2 = (5 > 3 && 2 < 1001) || (7 == 8); // TRUE OR FALSE --> TRUE
//
//        int[] in1 = {5, 91, 304, 405};
//        double[] da1 = new double[3];
//        da1[0] = 56.7;
//        da1[1] = 45.8;
//        da1[2] = 91.2;
//
//        char chara1 = 'a';
//        short chara2 = 'a';
//        String chara3 = "a";
//
//        String[] sa1 = {
//                "see on esimene väärtus",
//                "67",
//                "58.92"
//        };
//
//        // BigDecimal - komakohaga arvud, BigInteger = täisarvud.
//
//        BigInteger bigNumber = new BigInteger("765464644564564444444455555555555555555555555");
//        bigNumber = bigNumber.multiply(new BigInteger("2"));
//
//        int birthYear1 = retrieveBirthYear("3810424729");
//        int birthYear2 = retrieveBirthYear("2450711111");
//
//        System.out.println(birthYear1);
//        System.out.println(retrieveBirthYear("2450711111"));
//
//        System.out.println("Korrektrne isikukood: " + isPersonalCodeCorrect("3810424729"));
//        System.out.println("Ebakorrektne isikukood " + isPersonalCodeCorrect("38114242729"));
//
//        // 3 - 20. sajandil sündinud mees, 4 - naine
//        // 81 - aasta sajandil
//        // 04- kuu
//        // 24 - kuupäev
//        // 27 - haigla, kus sündis
//        // 2 - mitmes sündija, sellel päeval
//        // 9 - kontrollnumber
//
//
//    }
//
//    private static int retrieveBirthYear(String personalCode) {
//        // 38104242729 --> 1981
//
//        int centuryKey = Integer.parseInt(personalCode.substring(0, 1));
//        int centuryYear = Integer.parseInt(personalCode.substring(1, 3));
//
//        int century;
//        switch (centuryKey) {
//            case 1:
//            case 2:
//                century = 1800;
//                break;
//            case 3:
//            case 4:
//                century = 1900;
//                break;
//            case 5:
//            case 6:
//                century = 2000;
//                break;
//            case 7:
//            case 8:
//                century = 2100;
//            default:
//                century = 0;
//        }
//                return century + centuryYear;
//        }
//
//
//        private static boolean isPersonalCodeCorrect(String personalCode) {
////        if (personalCode == null) || personalCode.length() !=11) {
////    return false;
//            }
//                    // Muudame isikukoodi teksti täisarvude massiiviks.
//            int[] personalCodeDigits = new int[10];
//    char[] personalCodeChars = personalCode.toCharArray();
//    for (int i = 0; i < 10; i++) {
//        String digitString = String.valueOf(personalCodeChars[i]);
//        int digit = Integer.parseInt(digitString);
//        personalCodeDigits[i] = digit;
//    }
//
//    int[] weights1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};
//
//    int sum = 0;
//    for(int i = 0; i < 10; i++) {
//        sum = sum + weights1[i] + personalCodeDigits[i];
//}
//    int checkNumber = 0;
//    if (sum % 11 != 10) {
//        checkNumber = sum % 11;
//            } else {
//        int[] weights2 = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};
//        sum = 0;
//        for (int i = 0; i < 10; i++) {
//            sum = sum + weights2[i] + personalCodeDigits[i];
//        }
//        if (sum % 11 != 10) {
//            checkNumber = sum % 11;
//        }
//    }
//    char personalCodeLastChar = personalCode.charAt(10);
//    String personalCodeLastCharStr = String.valueOf(personalCodeLastChar);
//        int personalCodeLastDigit = Integer.parseInt(personalCodeLastCharStr);
//
//        boolean isCorrect = personalCodeLastDigit == checkNumber;
//        return isCorrect;
//    }
//
//        }
//
//
//
//
