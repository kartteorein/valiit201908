package week2;

public class w2d6h {

    // 1

    public static void main(String[] args) {
        test(3);
        test2(null, "mingi tekst");
        System.out.println(addVat(100.0));

        String gender = deriveGender("496...");
        System.out.println("Sugu: " + gender);

    }
    static boolean test(int someNumber) {
        return false;
    }

    // 2, 3

    static void test2(String text1, String text2) {

    }

    static double addVat(double initialProce) {
        return initialProce * 1.2;
    }

    // 4

    static int[] generateArray(int a, int b, boolean alphabetical) {
        if (alphabetical) {
            return new int[]{a, b};
        } else {
            return new int[]{b, a};
        }
    }

    // 5

    static String deriveGender(String personalCode) {
        char firstDigitChar = personalCode.charAt(0);
        String firstDigitStr = String.valueOf(firstDigitChar);
        int firstDigitValue = Integer.parseInt(firstDigitStr);

        return firstDigitValue % 2 == 1 ? "M" : "F";
               // if (firstDigitValue % 2 == 1) {
    }

    // 6

    static  void printHello() {
        System.out.println("Tere");
    }
}
